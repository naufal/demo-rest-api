<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = false;

    protected $casts = [
        'id' => 'string',
        'fixed_price' => 'double'
    ];
}