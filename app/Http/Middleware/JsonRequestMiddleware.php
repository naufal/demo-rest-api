<?php

namespace App\Http\Middleware;

use App\Response;
use Closure;
use Illuminate\Http\Request;

class JsonRequestMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->has('data')) {
            try {
                $data = json_decode($request->input('data'));
            } catch (\Exception $e) {
                return Response::badRequest('Error when parsing your request.');
            }
            foreach ($data as $key => $value) {
                $request->request->add([$key => $value]);
            }
            return $next($request);
        }
        return Response::badRequest('Please provide data into the header request.');
    }

}