<?php

namespace App\Http\Controllers;

use App\ActiveLogin;
use App\Response;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->email = $request->input('email');
        $user->phone_number = $request->input('phone_number');
        $user->address = $request->input('address');
        $user->balance = 0;
        $user->password = User::generatePassword($user->name, $request->input('password'));
        $success = $user->save();
        if ($success) {
            return Response::success('Registration success.');
        }
        return Response::badRequest('Registration failed.');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('email', $request->input('email'))->get()->first();
        if ($user->password === User::generatePassword($user->name, $request->input('password'))) {
            $data = new \StdClass();
            $data->token = ActiveLogin::generateToken($user->email);
            $active_login = new ActiveLogin();
            $active_login->email = $user->email;
            $active_login->token = $data->token;
            $active_login->save();
            return Response::success('Login success.', $data);
        }
        return Response::badRequest('Login failed.');
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
        ]);
        try {
            $active_login = ActiveLogin::where('token', $request->input('token'))->get()->first();
            $active_login->delete();
            return Response::success('Logout success.');
        } catch (\Exception $e) {
            return Response::badRequest();
        }
    }
}
