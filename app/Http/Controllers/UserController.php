<?php

namespace App\Http\Controllers;

use App\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $user = $request->user();
        return Response::success('Get profile success.', $user);
    }
}
