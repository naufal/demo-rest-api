<?php

namespace App\Http\Controllers;

use App\Response;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('type')) {
            $result = Service::where('category', $request->input('type'))->get();
            $count = Service::where('category', $request->input('type'))->count();
            $message = 'Get service list for ' . $request->input('type') . ' ';
            if ($count > 0)
                return Response::success($message . 'success.', $result);
            else
                return Response::badRequest($message . 'failed.');
        }
        return Response::success('Get service list success.', Service::all());
    }
}
