<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveLogin extends Model
{
    public static function generateToken($username)
    {
        return sha1($username . date_default_timezone_get() . str_random(4));
    }
}