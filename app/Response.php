<?php

namespace App;

use Illuminate\Http\JsonResponse;

class Response
{
    public $status;
    public $message;
    public $data;

    public static function success($message = 'Success.', $data = null)
    {
        $response = new Response();
        $response->status = 200;
        $response->message = $message;
        $response->data = $data;
        return JsonResponse::create($response, 200);
    }

    public static function badRequest($message = 'Failed.', $data = null)
    {
        $response = new Response();
        $response->status = 400;
        $response->message = $message;
        $response->data = $data;
        return JsonResponse::create($response, 400);
    }

    public static function unauthorized()
    {
        $response = new Response();
        $response->status = 401;
        $response->message = 'Unauthorized';
        $response->data = null;
        return JsonResponse::create($response, 401);
    }
}
