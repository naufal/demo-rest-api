<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api'], function () use ($app) {
    $app->group(['prefix' => 'service'], function () use ($app) {
        $app->post('list', 'ServiceController@index');
    });
    $app->group(['prefix' => 'auth', 'middleware' => 'json_request'], function () use ($app) {
        $app->post('register', 'AuthController@register');
        $app->post('login', 'AuthController@login');
        $app->post('logout', ['middleware' => 'auth', 'uses' => 'AuthController@logout']);
    });
    $app->group(['prefix' => 'user', 'middleware' => ['json_request', 'auth']], function () use ($app) {
        $app->post('profile', 'UserController@profile');
    });
});
