<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('services')->insert([
            'id' => 'listrik-50',
            'name' => 'Token Listrik 50 Ribu',
            'fixed_price' => 52000.00,
            'category' => 'electric'
        ]);
        DB::table('services')->insert([
            'id' => 'listrik-100',
            'name' => 'Token Listrik 100 Ribu',
            'fixed_price' => 101000.00,
            'category' => 'electric'
        ]);
        DB::table('services')->insert([
            'id' => 'listrik-150',
            'name' => 'Token Listrik 150 Ribu',
            'fixed_price' => 151000.00,
            'category' => 'electric'
        ]);
        DB::table('services')->insert([
            'id' => 'listrik-200',
            'name' => 'Token Listrik 200 Ribu',
            'fixed_price' => 200500.00,
            'category' => 'electric'
        ]);
        DB::table('services')->insert([
            'id' => 'air-50',
            'name' => 'Token PDAM 50 Ribu',
            'fixed_price' => 52000.00,
            'category' => 'water'
        ]);
        DB::table('services')->insert([
            'id' => 'air-100',
            'name' => 'Token PDAM 100 Ribu',
            'fixed_price' => 101000.00,
            'category' => 'water'
        ]);
        DB::table('services')->insert([
            'id' => 'air-150',
            'name' => 'Token PDAM 150 Ribu',
            'fixed_price' => 151000.00,
            'category' => 'water'
        ]);
        DB::table('services')->insert([
            'id' => 'air-200',
            'name' => 'Token PDAM 200 Ribu',
            'fixed_price' => 200500.00,
            'category' => 'water'
        ]);
        DB::table('services')->insert([
            'id' => 'tv-first-media',
            'name' => 'First Media',
            'fixed_price' => 152000.00,
            'category' => 'cable-tv'
        ]);
        DB::table('services')->insert([
            'id' => 'tv-telkom-vision',
            'name' => 'Telkom Vision',
            'fixed_price' => 163000.00,
            'category' => 'cable-tv'
        ]);
        DB::table('services')->insert([
            'id' => 'tv-yes-tv',
            'name' => 'Yes TV',
            'fixed_price' => 133000.00,
            'category' => 'cable-tv'
        ]);
        DB::table('services')->insert([
            'id' => 'tv-orange-tv',
            'name' => 'Orange TV',
            'fixed_price' => 99000.00,
            'category' => 'cable-tv'
        ]);
        DB::table('services')->insert([
            'id' => 'pulse-20',
            'name' => 'Electric Voucher All Op. 25 Ribu',
            'fixed_price' => 27000.00,
            'category' => 'pulse'
        ]);
        DB::table('services')->insert([
            'id' => 'pulse-50',
            'name' => 'Electric Voucher All Op. 50 Ribu',
            'fixed_price' => 51000.00,
            'category' => 'pulse'
        ]);
        DB::table('services')->insert([
            'id' => 'pulse-100',
            'name' => 'Electric Voucher All Op. 100 Ribu',
            'fixed_price' => 100500.00,
            'category' => 'pulse'
        ]);
        DB::table('services')->insert([
            'id' => 'pulse-150',
            'name' => 'Electric Voucher All Op. 150 Ribu',
            'fixed_price' => 149500.00,
            'category' => 'pulse'
        ]);
    }
}